# Generated by Django 5.0.1 on 2024-01-13 04:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
        ('genres', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmodel',
            name='genres',
            field=models.ManyToManyField(to='genres.genremodel'),
        ),
    ]
