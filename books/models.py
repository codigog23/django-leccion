from django.db import models
from authors.models import AuthorModel
from genres.models import GenreModel


# Create your models here.
class BookModel(models.Model):
    title = models.CharField(max_length=160)
    # 1 a Muchos
    author = models.ForeignKey(AuthorModel, on_delete=models.CASCADE)
    # Muchos a Muchos
    genres = models.ManyToManyField(GenreModel)

    class Meta:
        db_table = 'books'
