from django.contrib import admin
from .models import BookModel


# Register your models here.
@admin.register(BookModel)
class BookAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author']
    search_fields = ['title', 'author']
    list_filter = ['author']
