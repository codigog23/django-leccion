from django.db import models
from authors.models import AuthorModel


class AuthorProfileModel(models.Model):
    biography = models.TextField()
    author = models.OneToOneField(AuthorModel, on_delete=models.CASCADE)

    class Meta:
        db_table = 'authors_profile'
