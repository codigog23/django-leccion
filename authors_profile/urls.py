from rest_framework.routers import DefaultRouter
from .views import AuthorProfileViewset

router = DefaultRouter()
router.register('', AuthorProfileViewset, basename='author_profile')

urlpatterns = router.urls
