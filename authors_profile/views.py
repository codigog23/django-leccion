from rest_framework import viewsets
from .models import AuthorProfileModel
from .serializers import AuthorProfileSerializer


class AuthorProfileViewset(viewsets.ModelViewSet):
    # ForeignKey - OneToOneField -> select_related
    # OneToManyField - ManyToManyField -> prefetch_related
    queryset = AuthorProfileModel.objects.select_related('author').all()
    serializer_class = AuthorProfileSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        print(queryset.query)
        return queryset
