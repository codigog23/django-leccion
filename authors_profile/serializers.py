from rest_framework import serializers
from .models import AuthorProfileModel


class AuthorProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthorProfileModel
        fields = ['id', 'biography', 'author']
        depth = 1
