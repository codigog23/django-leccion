# Django

## Caracteristicas

1. MTV (Model Template View)
2. ORM propio
3. Administración
4. Desarollo Rapido
5. Seguridad
6. Escalabilidad y Versatilidad

## Instalación

```sh
pipenv install Django
```

## Iniciar Django

- Crear un proyecto

```sh
django-admin startproject <nombre_proyecto> .
```

- Iniciar proyecto

```sh
python manage.py runserver
```

- Crear superusuario (Se ejecuta despues de las migraciones)

```sh
python manage.py createsuperuser
```

## Apps

1. Modularidad
2. Reutilización
3. Desacoplamiento
4. Escalabilidad
5. Enfoque en la funcionalidad

- Crear una app

```sh
python manage.py startapp <nombre_app>
```

## Migraciones

- Sincronizar o Aplicar migraciones

```sh
python manage.py migrate
```

- Crear una migración

```sh
python manage.py makemigrations
python manage.py makemigrations <nombre_app>
```

## Modulos

- Authors

| columna | tipo            |
| ------- | --------------- |
| id      | integer PK auto |
| name    | varchar(100)    |

- Books

| columna   | tipo                    |
| --------- | ----------------------- |
| id        | integer PK auto         |
| title     | varchar(160)            |
| author_id | integer FK (authors.id) |

- Genres

| columna | tipo            |
| ------- | --------------- |
| id      | integer PK auto |
| name    | varchar(100)    |

- BooksGenres (Pivote)

| columna  | tipo                   |
| -------- | ---------------------- |
| id       | integer PK auto        |
| genre_id | integer FK (genres.id) |
| book_id  | integer FK (books.id)  |

## Environment (.env)

```py
DEBUG=True

DB_NAME='django_lesson'
DB_USER='postgres'
DB_PASSWORD='mysql'
DB_HOST='127.0.0.1'
DB_PORT='5432'
```
