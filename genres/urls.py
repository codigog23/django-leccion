# from django.urls import path
# from .views import GenreListCreate
# 
# urlpatterns = [
#     path('', GenreListCreate.as_view())
# ]
from rest_framework.routers import DefaultRouter
from .views import GenreViewset

router = DefaultRouter()
router.register('', GenreViewset, basename='genres')


urlpatterns = router.urls
