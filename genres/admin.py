from django.contrib import admin
from .models import GenreModel


# Register your models here.
@admin.register(GenreModel)
class GenreAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    search_fields = ['name']
