from rest_framework import generics, viewsets
from .models import GenreModel
from .serializers import GenreSerializer


# class GenreListCreate(generics.ListCreateAPIView):
#     queryset = GenreModel.objects.all()
#     serializer_class = GenreSerializer

class GenreViewset(viewsets.ModelViewSet):
    queryset = GenreModel.objects.all()
    serializer_class = GenreSerializer
