from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import AuthorModel
from .serializers import AuthorSerializer

# /authors/ -> GET | Obtiene los autores
# /authors/ -> POST | Crear autor
# /authors/{id} -> GET | Obtener un autor por el ID
# /authors/{id} -> PUT - PATH | Actualiza un autor por el ID
# /authors/{id} -> DELETE | Eliminar un autor por el ID 


@api_view(['GET', 'POST'])
def author_list(request): # all
    if request.method == 'GET':
        # self.model.all()
        records = AuthorModel.objects.all()
        # self.schema(records, many=True)
        serializer = AuthorSerializer(records, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        serializer = AuthorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
